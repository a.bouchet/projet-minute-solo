ALTER TABLE ONLY "raid_classement"."activite_raid"
    ADD CONSTRAINT "pk_activite_raid" PRIMARY KEY ("code_raid", "code_activite");

ALTER TABLE ONLY "raid_classement"."departement"
    ADD CONSTRAINT "pk_departement" PRIMARY KEY ("numero");

ALTER TABLE ONLY "raid_classement"."activite"
    ADD CONSTRAINT "pk_activite" PRIMARY KEY ("code");

ALTER TABLE ONLY "raid_classement"."coureur"
    ADD CONSTRAINT "pk_coureur" PRIMARY KEY ("licence");

ALTER TABLE ONLY "raid_classement"."equipe"
    ADD CONSTRAINT "pk_equipe" PRIMARY KEY ("numero", "code_raid");

ALTER TABLE ONLY "raid_classement"."integrer_equipe"
    ADD CONSTRAINT "pk_integrer" PRIMARY KEY ("numero_equipe", "code_raid", "licence");

ALTER TABLE ONLY "raid_classement"."raid"
    ADD CONSTRAINT "pk_raid" PRIMARY KEY ("code");

ALTER TABLE ONLY "raid_classement"."traverser"
    ADD CONSTRAINT "pk_traverser" PRIMARY KEY ("code_raid", "numero_dept");

ALTER TABLE ONLY "raid_classement"."activite_raid"
    ADD CONSTRAINT "activite_raid_code_activite_fkey" FOREIGN KEY ("code_activite") REFERENCES "raid_classement"."activite"("code");

ALTER TABLE ONLY "raid_classement"."activite_raid"
    ADD CONSTRAINT "activite_raid_code_raid_fkey" FOREIGN KEY ("code_raid") REFERENCES "raid_classement"."raid"("code");

ALTER TABLE ONLY "raid_classement"."equipe"
    ADD CONSTRAINT "equipe_code_raid_fkey" FOREIGN KEY ("code_raid") REFERENCES "raid_classement"."raid"("code");

ALTER TABLE ONLY "raid_classement"."integrer_equipe"
    ADD CONSTRAINT "inscrire_individuel_code_coureur_fkey2" FOREIGN KEY ("licence") REFERENCES "raid_classement"."coureur"("licence");

ALTER TABLE ONLY "raid_classement"."integrer_equipe"
    ADD CONSTRAINT "inscrire_individuel_equipe_fkey" FOREIGN KEY ("numero_equipe", "code_raid") REFERENCES "raid_classement"."equipe"("numero", "code_raid");

ALTER TABLE ONLY "raid_classement"."traverser"
    ADD CONSTRAINT "traverser_code_dept_fkey" FOREIGN KEY ("numero_dept") REFERENCES "raid_classement"."departement"("numero");

ALTER TABLE ONLY "raid_classement"."traverser"
    ADD CONSTRAINT "traverser_code_raid_fkey" FOREIGN KEY ("code_raid") REFERENCES "raid_classement"."raid"("code");
