SET client_encoding = 'UTF8';
DROP SCHEMA "raid_classement" CASCADE;
CREATE SCHEMA "raid_classement";

CREATE TABLE "raid_classement"."activite" (
    "code" character varying(5) NOT NULL,
    "libelle" character varying(50)
);
CREATE TABLE "raid_classement"."activite_raid" (
    "code_raid" character varying(5) NOT NULL,
    "code_activite" character varying(5) NOT NULL
);
CREATE TABLE "raid_classement"."coureur" (
    "licence" character varying(5) NOT NULL,
    "nom" character varying(50),
    "prenom" character varying(50),
    "sexe" character(1),
    "nationalite" character varying(50),
    "mail" character varying(50),
    "certif_med_aptitude" boolean DEFAULT false,
    "datenaiss" "date"
);
CREATE TABLE "raid_classement"."departement" (
    "numero" integer NOT NULL,
    "nom" character varying(30)
);
CREATE TABLE "raid_classement"."equipe" (
    "numero" integer NOT NULL,
    "code_raid" character varying(5) NOT NULL,
    "nom" character varying(50),
    "dateinscription" "date" DEFAULT "now"(),
    "temps_global" integer DEFAULT 0,
    "classement" integer DEFAULT 0,
    "penalites_bonif" integer DEFAULT 0
);
CREATE TABLE "raid_classement"."integrer_equipe" (
    "numero_equipe" integer NOT NULL,
    "code_raid" character varying(5) NOT NULL,
    "licence" character varying(5) NOT NULL,
    "temps_individuel" integer DEFAULT 0,
    "num_dossard" character varying(15)
);
CREATE TABLE "raid_classement"."raid" (
    "code" character varying(5) NOT NULL,
    "nom" character varying(50),
    "datedebut" character varying(10),
    "ville" character varying(50),
    "region" character varying(50),
    "nb_maxi_par_equipe" integer,
    "montant_inscription" numeric(8,2),
    "nb_femmes" integer,
    "duree_maxi" integer,
    "age_minimum" integer
);
CREATE TABLE "raid_classement"."traverser" (
    "code_raid" character varying(5) NOT NULL,
    "numero_dept" integer NOT NULL
);
